# LS Task 5

NoSQL Database

> Name: Ruben Brouwers <br>
> Group: ACS201

## SQL Query

```SQL
SELECT TOP (350)
    f.id 'FLIGHT_ID',
    f.date 'FLIGHT_DATE',
    f.name 'FLIGHT_NAME',
    arr.id 'ARR_AIRPORT.id',
    arr.name 'ARR_AIRPORT.name',
    arr.iso_country 'ARR_AIRPORT.countryIso',
    dep.id 'DEP_AIRPORT.id',
    dep.name 'DEP_AIRPORT.name',
    dep.iso_country 'DEP_AIRPORT.countryIso',
    (
        SELECT dt.code 'DELAY_TYPE', d.minutes 'DELAY_LENGTH'
        FROM delays d
        JOIN delaytypes dt ON d.delay_type_id = dt.id
        WHERE d.flight_id = f.id
        FOR JSON PATH
    ) 'DELAYS'
FROM flights f
JOIN airports dep ON f.departure_id = dep.id
JOIN airports arr ON f.arrival_id = arr.id
ORDER BY f.id
FOR JSON PATH
```

### Result

```JSON
[
    {
        "FLIGHT_ID": "9EA78385-0DAB-864E-B70D-000005E72797",
        "FLIGHT_DATE": "1994-05-05",
        "FLIGHT_NAME": "KMC119525",
        "ARR_AIRPORT": {
            "id": "ZYMH",
            "name": "Gu-Lian Airport",
            "countryIso": "CN"
        },
        "DEP_AIRPORT": {
            "id": "LGSP",
            "name": "Sparti Airport",
            "countryIso": "GR"
        }
    },
    {
        "FLIGHT_ID": "DAF25BC2-3175-F24C-89F0-000006BA837D",
        "FLIGHT_DATE": "2008-07-03",
        "FLIGHT_NAME": "DHK215",
        "ARR_AIRPORT": {
            "id": "OEDR",
            "name": "King Abdulaziz Air Base",
            "countryIso": "SA"
        },
        "DEP_AIRPORT": {
            "id": "TRPG",
            "name": "John A. Osborne Airport",
            "countryIso": "MS"
        },
        "DELAYS": [
            {
                "DELAY_TYPE": "AE",
                "DELAY_LENGTH": 1...
```

---

## Database creation

### The folder structure

```bash
PS C:\LowStake> tree
Folder PATH listing
Volume serial number is 60DB-5C54
C:.
├───rsconfig
│   └───rep1
├───rsShard1
│   ├───rep1
│   ├───rep2
│   └───rep3
└───rsShard2
    ├───rep1
    ├───rep2
    └───rep3
```

### Making MongoDB

#### Ports

- Config
  - 31001
- Router
  - 27020
- Shard 1
  - rs1 : 30011
  - rs2 : 30012
  - rs3 : 30013
- Shard 2
  - rs1 : 30021
  - rs2 : 30022
  - rs3 : 30023

#### rsConfig

Make rsConfig server:

```bash
mongod --configsvr --replSet "configrs" --dbpath C:\LowStake\rsconfig\rep1\ --port 31001
```

Connecting to rsConfig server:

```bash
mongosh --host localhost --port 31001
```

Initiating the config server:

```bash
rs.initiate( { _id: "configrs", configsvr: true, members: [ { _id: 0, host: "localhost:31001" } ] } )
```

#### Shard 1

Make rsShard1 rs1:

```bash
mongod --shardsvr --replSet "rsShard1" --dbpath c:/LowStake/rsShard1/rep1 --port 30011
```

Make rsShard1 rs2:

```bash
mongod --shardsvr --replSet "rsShard1" --dbpath c:/LowStake/rsShard1/rep2 --port 30012
```

Make rsShard1 rs3:

```bash
mongod --shardsvr --replSet "rsShard1" --dbpath c:/LowStake/rsShard1/rep3 --port 30013
```

Connect to rsShard1:

```bash
mongosh --host localhost --port 30011
```

Initiate rsShard1:

```bash
rs.initiate({ _id: "rsShard1", members: [ { _id: 0, host: "localhost:30011" },  { _id: 1, host: "localhost:30012" },  { _id: 2, host: "localhost:30013" } ] } )
```

#### Shard 2

Make rsShard2 rs1:

```bash
mongod --shardsvr --replSet "rsShard2" --dbpath c:/LowStake/rsShard2/rep1 --port 30021
```

Make rsShard2 rs2:

```bash
mongod --shardsvr --replSet "rsShard2" --dbpath c:/LowStake/rsShard2/rep2 --port 30022
```

Make rsShard2 rs3:

```bash
mongod --shardsvr --replSet "rsShard2" --dbpath c:/LowStake/rsShard2/rep3 --port 30023
```

Connect to rsShard2:

```bash
mongosh --host localhost --port 30021
```

Initiate rsShard2:

```bash
rs.initiate({ _id: "rsShard2", members: [ { _id: 0, host: "localhost:30021" },  { _id: 1, host: "localhost:30022" },  { _id: 2, host: "localhost:30023" } ] } )
```

#### Router

Make router:

```bash
mongos --configdb rs1/localhost:31001 --port 27020
```

Connect to router:

```bash
mongosh --host localhost --port 27020
```

Connecting the shards:

```bash
sh.addShard("rsShard1/localhost:30011")
sh.addShard("rsShard2/localhost:30021")
```

Enable sharding:
```bash
sh.enableSharding("flightmanagement")
```

##### Configuration

Set the default chunk size to 1MB:

```bash
use config
db.settings.insertOne({_id: "chunksize", value: 1 })
```

Creating a new db:

<img src="createDB.png"
     alt="Creating a new database"
     style="margin-right: 10px;" />

Enabling sharding with the set shard key:

```bash
use flightmanagement
db.flights.ensureIndex({FLIGHT_DATE:1})
sh.shardCollection("flightmanagement.flights",{"FLIGHT_DATE":1})
```

I was wondering if it would be possible to select the date and another value (departure airport) as a compostite shardkey, and I found [this](https://www.mongodb.com/docs/manual/core/index-multikey/?_ga=2.208260049.25035034.1652712665-1013187443.1651596415#shard-keys). Sadly I was not able to implement it on time...

```bash
db.flights.createIndex({"DEP_AIRPORT.countryIso":1, FLIGHT_DATE:1})
sh.shardCollection("flightmanagement.flights",{"DEP_AIRPORT.countryIso":1, FLIGHT_DATE:1})
```

<img src="ensureindexDepre.png"
     alt="Creating a new database"
     style="margin-right: 10px;" />

Checking the status of the sharding:

```bash
sh.status()
```

<img src="shardingProof.png"
     alt="Creating a new database"
     style="margin-right: 10px;" />


---
## Query

Find all the flights with a delay larger than 20 minutes and from the country Brazil
```bash
db.flights.find({'DEP_AIRPORT.countryIso': 'BR','DELAYS.DELAY_LENGTH': {$gt: 20}})
```

```bash
[
  {
    _id: ObjectId("62840125ea9b067b10d3194c"),
    FLIGHT_ID: '61B10B45-4D9A-F445-9D0F-00052B1E1355',
    FLIGHT_DATE: '2021-08-12',
    FLIGHT_NAME: 'TE186',
    ARR_AIRPORT: {
      id: 'OMAL',
      name: 'Al Ain International Airport',
      countryIso: 'AE'
    },
    DEP_AIRPORT: { id: 'SNXA', name: 'Machado Airport', countryIso: 'BR' },
    DELAYS: [ { DELAY_TYPE: 'FF', DELAY_LENGTH: 298 } ]
  },
  {
    _id: ObjectId("62840137ea9b067b10d31aaa"),
    FLIGHT_ID: '61B10B45-4D9A-F445-9D0F-00052B1E1355',
    FLIGHT_DATE: '2021-08-12',
    FLIGHT_NAME: 'TE186',
    ARR_AIRPORT: {
      id: 'OMAL',
      name: 'Al Ain Int...
```