/*What role do date parameters (days, weekday, months, season) have on the length of the effective delays? (eg. number of delays per month)*/
SELECT d.DAY_OF_MONTH, MAX(f.EFFECTIVE_DELAY_TIME_MV) AS 'Largest Effective Delay'
FROM dim_DATE AS d JOIN fact_DELAY AS f ON f.DIM_DAY_FK = d.DATE_SK
GROUP BY d.DAY_OF_MONTH 
ORDER BY d.DAY_OF_MONTH ASC;

SELECT d.MONTH, MAX(f.EFFECTIVE_DELAY_TIME_MV) AS 'Largest Effective Delay'
FROM dim_DATE AS d JOIN fact_DELAY AS f ON f.DIM_DAY_FK = d.DATE_SK
GROUP BY d.MONTH 
ORDER BY d.MONTH ASC;

/*What is the total delay time per weekday?*/
SELECT d.DAY_OF_WEEK, AVG(f.EFFECTIVE_DELAY_TIME_MV) AS 'Average Effective Delay'
FROM dim_DATE AS d JOIN fact_DELAY AS f ON f.DIM_DAY_FK = d.DATE_SK
GROUP BY d.DAY_OF_WEEK;

/*For which delay types is there usually an overestimation/underestimation of the delay?*/
SELECT dt.DELAY_CODE, dt.DELAY_DESCRIPTION,f.EFFECTIVE_DELAY_TIME_MV - f.ESTIMATE_DELAY_TIME_MV AS RATIO
FROM dim_DELAYTYPE AS dt JOIN fact_DELAY AS f ON f.DIM_DELAY_TYPE_FK = dt.DELAY_TYPE_SK
GROUP BY dt.DELAY_CODE
/*NEEDS WORK*/

--Solution Dominik 
/*select q.DELAY_DESCRIPTION, count(*) as counter, q.UNDER_OVERESTIMATION  
from (
    select ddt.DELAY_DESCRIPTION,
    case when f.ESTIMATE_DELAY_TIME_MV < f.EFFECTIVE_DELAY_MV
    then 'underestimated'
    when f.ESTIMATE_DELAY_TIME_MV = f.EFFECTIVE_DELAY_MV
    then 'no under/overestimation'
    else 'overestimated' end UNDER_OVERESTIMATION
    from fact_delay f join dim_delay_type ddt on (f.DIM_DELAY_TYPE_FK = ddt.DELAY_TYPE_SK)) q
group by q.UNDER_OVERESTIMATION, q.DELAY_DESCRIPTION
order by q.DELAY_DESCRIPTION;*/

/*Does the population in the country of departure affect departure delays?*/
SELECT a.COUNTRY_NAME, a.AIRPORT_NAME, a.POPULATION, f.EFFECTIVE_DELAY_TIME_MV
FROM dim_AIRPORTS AS a JOIN fact_DELAY AS f ON f.DIM_AIRPORT_DEPARTURE_FK = a.AIRPORT_SK
WHERE f.DIM_DELAY_MOMENT_FK = 1
ORDER BY EFFECTIVE_DELAY_TIME_MV DESC;

/*In which regions (eg. Europe, Asia ...) do the delays last the longest*/
SELECT REGION, MAX(f.EFFECTIVE_DELAY_TIME_MV) AS DELAY_TIME
FROM dim_AIRPORTS AS a JOIN fact_DELAY AS f ON f.DIM_AIRPORT_DEPARTURE_FK = a.AIRPORT_SK
GROUP BY REGION
ORDER BY DELAY_TIME DESC

/*Do flights with a 'long' duration have longer delays on average.*/
SELECT fl.DURATION, AVG(F.EFFECTIVE_DELAY_TIME_MV) AS 'Average Flight Duration'
FROM dim_FLIGHT AS fl JOIN fact_DELAY AS f ON fl.FLIGHT_SK = f.DIM_FLIGHT_FK
GROUP BY fl.DURATION
ORDER BY [Average Flight Duration] ASC

/*What is the impact of the flight duration (short < 3hrs, median 3-5 hrs, long >5hrs)  on the average delay duration?*/
/*Look at output script above*/

/*Own Analytical Questions-*/
 /*What is the country with the shortest arrival delay?*/
SELECT TOP(1) a.COUNTRY_NAME, MAX(f.EFFECTIVE_DELAY_TIME_MV) AS DELAY_TIME
FROM dim_AIRPORTS AS a JOIN fact_DELAY AS f ON f.DIM_AIRPORT_DEPARTURE_FK = a.AIRPORT_SK
WHERE f.DIM_DELAY_MOMENT_FK = 2
GROUP BY a.COUNTRY_NAME
ORDER BY DELAY_TIME ASC;

/*How many delays do we have in our system?*/
SELECT SUM(DELAY_COUNT_MV) AS 'AMOUNT OF DELAYS IN SYSTEM', dm.DELAY_MOMENT_DESC 
FROM fact_DELAY AS f JOIN dim_DELAYMOMENT AS dm ON f.DIM_DELAY_MOMENT_FK = dm.DELAY_MOMENT_SK
GROUP BY dm.DELAY_MOMENT_DESC;

/*What delay type has the longest duration*/
SELECT TOP(10) MAX(f.EFFECTIVE_DELAY_TIME_MV) AS 'Max Delay Time', dt.DELAY_CODE, dt.DELAY_DESCRIPTION
FROM fact_DELAY AS f JOIN dim_DELAYTYPE AS dt ON f.DIM_DELAY_TYPE_FK = dt.DELAY_TYPE_SK
GROUP BY dt.DELAY_CODE, dt.DELAY_DESCRIPTION
ORDER BY [Max Delay Time] DESC

/*What was the flight with the shortest delay?*/
SELECT TOP(100) fl.FLIGHT_NUMBER, f.EFFECTIVE_DELAY_TIME_MV
FROM fact_DELAY AS f JOIN dim_FLIGHT AS fl ON f.DIM_FLIGHT_FK = fl.FLIGHT_SK
ORDER BY f.EFFECTIVE_DELAY_TIME_MV ASC


